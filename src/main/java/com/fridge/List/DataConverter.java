package com.fridge.List;

import java.util.ArrayList;

public class DataConverter<T> {
    public DataConverter() {
    }

    public java.util.List convertToList(T[] arr) {
        java.util.List elements = new ArrayList<>();
        for (T el : arr) {
            elements.add(el);
        }
        return elements;
    }

    public DynamicArray<T> convertToDynamicArray(java.util.List<T> elements) {
        DynamicArray<T> arr = new DynamicArray();
        elements.forEach(el -> arr.add(el));
        return arr;
    }
}
