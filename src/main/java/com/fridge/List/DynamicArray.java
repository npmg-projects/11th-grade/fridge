package com.fridge.List;

public class DynamicArray<T> {
    private T[] arr;
    private int filled;

    public int getFilled() {
        return this.filled;
    }

    public T[] initArr(T[] arr, int capacity) {
        return (T[]) new Object[capacity];
    }

    public DynamicArray() {
        this.arr = (T[]) new Object[1];
        this.filled = 0;
    }

    public void add(T element) {
        if (arr.length == 0) {
            arr = initArr(arr, 1);
            arr[filled] = element;
            filled++;
            return;
        }
        if (filled == arr.length) {
            System.out.println("In");
            T newArr[] = initArr(arr, arr.length * 2);
            for (int i = 0; i < filled; i++) {
                newArr[i] = arr[i];
            }
            newArr[filled] = element;
            filled++;
            this.arr = initArr(arr, newArr.length);
            //setArr(newArr);
            for (int i = 0; i < filled; i++) {
                arr[i] = newArr[i];
            }
        } else {
            arr[filled] = element;
            filled++;
        }
    }

    public void add(int index, T element) {
        if (filled == arr.length) {
            T newArr[] = initArr(arr, arr.length * 2);
            for (int i = 0; i < index; i++) {
                newArr[i] = arr[i];
            }
            newArr[index] = element;
            for (int i = index + 1; i <= arr.length; i++) {
                newArr[i] = arr[i - 1];
            }
            this.arr = initArr(arr, newArr.length);
            //setArr(newArr);
            for (int i = 0; i < newArr.length; i++) {
                this.arr[i] = newArr[i];

            }
            //arr = newArr;
        } else {
            filled++;
            arr[filled] = element;
        }
    }

    public void remove(int index) {
        //O(n)
        T newArr[] = initArr(arr, arr.length - 1);
        for (int i = 0; i < index; i++) {
            newArr[i] = arr[i];
        }
        for (int i = index; i < newArr.length; i++) {
            newArr[i] = arr[i + 1];
        }
        arr = newArr;
    }

    public void addAll(T arr[]) {
        //O(n)
        T newArr[] = initArr(arr, this.arr.length + arr.length);
        for (int i = 0; i < this.arr.length; i++) {
            newArr[i] = this.arr[i];
        }
        for (int i = 0; i < arr.length; i++) {
            newArr[i + this.arr.length] = arr[i];
        }
        this.arr = newArr;
    }

    public int size() {
        return this.arr.length;
    }

    public T get(int index) {
        return this.arr[index];
    }

    public void set(int index, T element) {
        this.arr[index] = element;
    }
}
