package com.fridge.food;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FoodService {
    Food createFood(Food food) throws IOException;

    void removeFood(Integer foodId) throws IOException;

    Food editFood(Integer foodId, Food food) throws IOException;

    Food getFood(Integer foodId) throws IOException;

    List<Food> getFood() throws FileNotFoundException, IOException;
}
