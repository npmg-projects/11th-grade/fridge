package com.fridge.food;

import java.util.Date;

public class Food {
    private Integer id;

    private Date expire;

    private String name;

    private Integer count;

    public Food() {
    }

    public Food(Integer id, Date expire, String name, Integer count) {
        this.id = id;
        this.count = count;
        this.expire = expire;
        this.name = name;
    }

    public Date getExpire() {
        return expire;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", expire=" + expire +
                ", name='" + name + '\'' +
                ", count=" + count +
                '}';
    }
}
