package com.fridge.food;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FoodServiceImp implements FoodService {
    private ObjectMapper objectMapper;

    private final Random random;

    public FoodServiceImp() {
        this.objectMapper = new ObjectMapper();
        this.random = new Random();
    }

    private void configDB() {
        try {
            File myObj = new File("src/db/food.txt");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private String convertObjectToJSON(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object) + "\n";
    }

    private void fileExists(File file) throws FileNotFoundException {
        if (file == null) {
            throw new FileNotFoundException("404");
        }
    }

    private void overwriteDataInFile(List<Food> food) throws IOException {
        File file = new File("src/db/food.txt");
        fileExists(file);
        Path path = Path.of("src/db/food.txt");
        BufferedWriter writer = Files.newBufferedWriter(path);
        writer.write("");
        writer.flush();
        food.forEach(f -> {
            try {
                String foodJson = convertObjectToJSON(f);
                Files.write(path, foodJson.getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public Food createFood(Food food) throws IOException {
        configDB();
        Food newFood = new Food(random.nextInt(), food.getExpire(), food.getName(), food.getCount());
        File file = new File("src/db/food.txt");
        fileExists(file);
        String foodJson = convertObjectToJSON(newFood);
        Path path = Path.of("src/db/food.txt");
        Files.write(path, foodJson.getBytes(), StandardOpenOption.APPEND);

        return newFood;
    }

    @Override
    public void removeFood(Integer foodId) throws IOException {
        List<Food> food = getFood();
        food = food.stream().filter(f -> !f.getId().equals(foodId)).collect(Collectors.toList());
        overwriteDataInFile(food);
    }

    @Override
    public Food editFood(Integer foodId, Food food) throws IOException {
        Food editedFood = getFood(foodId);
        editedFood.setName(food.getName());
        editedFood.setCount(food.getCount());
        editedFood.setExpire(food.getExpire());
        List<Food> meals = getFood();
        meals = meals.stream().filter(f -> !f.getId().equals(foodId)).collect(Collectors.toList());
        meals.add(editedFood);
        overwriteDataInFile(meals);
        return food;
    }

    @Override
    public Food getFood(Integer foodId) throws IOException {
        List<Food> food = getFood();
        return food.stream().filter(f -> Objects.equals(f.getId(), foodId)).findAny().get();
    }

    @Override
    public List<Food> getFood() throws IOException {
        List<Food> objectsList = new ArrayList<>();
        Stream<String> stream = Files.lines(Paths.get("src/db/food.txt"));
        stream.forEach(json -> {
            try {
                objectsList.add(objectMapper.readValue(json, Food.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return objectsList;
    }
}
